$(document).ready(function () {

  /**********************
    **********************
      BURGER MENU
    **********************
    **********************/
  $(".burger__btn").click(function () {
    $("html").addClass("open");

    $(".menu__list").addClass("active");
  });

  $(".close__btn").click(() => {
    $("html").removeClass("open");

    $(".menu__list").removeClass("active");
  });

  /**********************
     **********************
        LINKS ONCLICK SCROLLING
     **********************
     **********************/
  let links = document.querySelectorAll('a[href="#"]');

  links.forEach((link) => {
    link.addEventListener("click", function (event) {
      event.preventDefault();
    });
  });

  /**********************
  **********************
    PRODUCTS SLIDER
  **********************
  **********************/

  $('.products__slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    prevArrow: $('.arrow__prev'),
    nextArrow: $('.arrow__next')
  });

  /**********************
  **********************
    TEAM SLIDER
  **********************
  **********************/

  $('.team__slider').slick({
    vertical: true,
    verticalSwiping: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: true,
  });



  /**********************
    **********************
      ON SCROLL ANIMATION
    **********************
    **********************/
  const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry => {
      console.log(entry)
      if (entry.isIntersecting) {
        entry.target.classList.add('show');
      } else {
        entry.target.classList.remove('show');
      }
    }))
  })
  const hiddenElements = document.querySelectorAll('.hidden');
  hiddenElements.forEach((el) => observer.observe(el));

  /**********************
   **********************
     ACCORDION
   **********************
   **********************/

  const accordionTitles = document.querySelectorAll(".accardion-btn");

  accordionTitles.forEach((accordionTitle) => {

    accordionTitle.addEventListener("click", () => {
      if (accordionTitle.classList.contains("accardion-active")) {
        accordionTitle.classList.remove("accardion-active");
      } else {

        const accordionTitlesWithIsOpen =
          document.querySelectorAll(".accardion-active");

        accordionTitlesWithIsOpen.forEach((accordionTitleWithIsOpen) => {
          accordionTitleWithIsOpen.classList.remove("accardion-active");
        });

        accordionTitle.classList.add("accardion-active");
      }
    });
  });


  /**********************
   **********************
     TABS
   **********************
   **********************/
  const tabs = document.querySelectorAll(".tab-link");
  const all_list = document.querySelectorAll(".tab-content");

  tabs.forEach((tab, index) => {
    tab.addEventListener("click", function () {

      tabs.forEach((tab) => {
        tab.classList.remove("active");
      });
      tab.classList.add("active");

      all_list.forEach((list) => {
        list.classList.remove("active");
      });
      all_list[index].classList.add("active");

      console.log(index);

    });
  });


  /**********************
    **********************
      VIDEO
    **********************
    **********************/
  $(".interview__item").magnificPopup({
    type: "iframe",
    mainClass: "mfp-fade",
    removalDelay: 160,
    preloader: false,
  });

  /* Video Preview */
  var Youtube = (function () {
    var video, results;

    var getThumb = function (url, size) {
      if (url === null) {
        return "";
      }
      size = (size === null) ? "big" : size;
      results = url.match("[\\?&]v=([^&#]*)");
      video = (results === null) ? url : results[1];

      if (size === "small") {
        return "https://img.youtube.com/vi/" + video + "/2.jpg";
      }
      return "https://img.youtube.com/vi/" + video + "/0.jpg";
    };

    return {
      thumb: getThumb
    };
  }());

  //Implementing
  let popupVideo = document.querySelectorAll(".interview__item");
  let galleryVideoItems = document.querySelectorAll(".interview__item");

  popupVideo.forEach((video, index) => {
    let link = video.href;

    let clearLink = link.replace("https://www.youtube.com/watch?v=", "")

    let thumb = Youtube.thumb(clearLink, "big");
    let thumbImg = document.createElement("img");
    thumbImg.src = thumb;

    galleryVideoItems[index].append(thumbImg);
  })
});

/**********************
   **********************
     PRODUCTS SLIDER
   **********************
   **********************/


/**********************
**********************
  READY TO ROCK
**********************
**********************/
const lenis = new Lenis();

lenis.on('scroll', (e) => {
  /*  console.log(e) */
})

function raf(time) {
  lenis.raf(time)
  requestAnimationFrame(raf)
}

requestAnimationFrame(raf)
