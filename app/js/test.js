
/*************************
  *************************
    OBJECTS
  *************************
*************************/
let city = "Paris"

const myCity = {
  city
}

let contryProp = 'country';
myCity[contryProp] = 'USA';

let popularity = 'Well hell yeah my baby';
myCity[popularity] = 'Gang it! It\'s already 3000 people!';

delete myCity.country

/* console.log(myCity); */

/* window.console.log(window.innerHeight); */
const cityDamn = {
  city,
  cityGreeting: function () {
    console.log('Hello World!');
  }
}
/* cityDamn.cityGreeting(); */
let cityJSON = JSON.stringify(myCity);
let cityParsed = JSON.parse(cityJSON);

/* console.log(cityJSON) */

const person1 = {
  name: "Owtp",
  age: 20,
  hobbies: {
    games: true,
    createMusic: true,
    listenToMusic: true
  }
}

/* Copying the object and can make unique changes, without changing the main object's link, but that changes will be applied to the next level inner objects */
const person2 = Object.assign({}, person1);
person2.name = "Drido"
person2.age = 21;

/* Same as previous, but shorter */
const person3 = { ...person1 }
person3.name = "Grubert"
person3.hobbies.createMusic = false;

/* Totally brand new copied object, even inner objects are stayed unique for it's own, and do not touching the main object's link */
const person4 = JSON.parse(JSON.stringify(person1))
person4.name = "Betty"
person4.hobbies.games = false;

console.log(person1);
console.log(person2);
console.log(person3);
console.log(person4);



/*************************
  *************************
    FUNCTIONS
  *************************
*************************/

/* let myFn = function (a , b) {
  let addition = a + 5;
  let correction = a + b;
  let c = correction;

  return c;
} */

function myFn(a, b) {
  let addition = a + 5;
  let correction = a + b;
  let c = correction;

  return c;
}

/* console.dir(myFn); */

/* Sending the info through link */

const personOne = {
  age: 20,
  name: "John"
}

function increasePersonAge(person) {
  person.age = person.age + 1;
  /*  return person */
}

increasePersonAge(personOne);
console.log(personOne.age)


function increasePersonAge2(person) {
  const updatedPerson = Object.assign({}, person);

  updatedPerson.age += 1;
  return updatedPerson;
}

const updatedPersonAge2 = increasePersonAge2(personOne);
console.log(updatedPersonAge2.age)



/* Callback */
function anotherFn() {
  console.log("Hello World!");
}

function fnWithCallback(call) {
  call();
}

fnWithCallback(anotherFn);

function printMyName() {
  console.log('Owtp');
}

/* setTimeout(printMyName, 2000); */


/* Area of visibility */
let f;
let b;

function areaFn() {
  let b
  f = true /* This is wrong, do not change the variable that is located outside of the function */
  b = "Bro, you should learn some stuff"
  console.log(b)
}

areaFn();

console.log(f) /* The variable a would be changed to true */
console.log(b)

const num = 5;

function fnIsMine() {
  function innerFn() {
    console.log(num);
  }
  innerFn();
}

fnIsMine();


/* Block in js is the any code that is placed between the curly brackets */

/* With the use strict, it will get the error, but it actually just wrong way to give a value to variable */

/* function wrongFn() {
  a = true;
  console.log(a);
}

wrongFn();

console.log(a); */

/* const btn = {
  width: 200,
  text: "More"
}

const redBtn = {
  ...btn,
  color: 'Red'
}

console.log(redBtn); */


/*************************
  *************************
    CONCATINATIONS
  *************************
*************************/


console.log('Hello ' + 'World!');
const hello = 'Hello';
const world = 'World!';

const greeting = hello + ' ' + world;
console.log(greeting);

const secondWayOfGreetings = `${hello} ${world}`;
console.log(secondWayOfGreetings);


/*************************
  *************************
    FUNCTIONS EXPRESSION
  *************************
*************************/

const queryJ = function (a, b) {
  let c;
  a = a + 1;
  c = a + b;

  console.log(c)
  return c;
}


console.log()

/* setTimeout(function () {
  console.log('Отложенное сообщение')
}, 500)
 */

/*************************
  *************************
    ARROWED FUNCTIONS
  *************************
*************************/

const gringo = a => a + b;


/*************************
  *************************
    DEFEAULT VALUES IN 
    PARAMETER IN FUNCTION
  *************************
*************************/

function multiplier(value, multiplier = 1) {
  return value * multiplier;
}
console.log(multiplier(6, 2));

console.log(multiplier(9));

const multiplier2 = (value, multiplier = 2) => {
  return value * multiplier;
}

console.log(multiplier2(5));

const newPost = (post, addedAt = Date()) => ({
  ...post,
  addedAt,
})

const firstPost = {
  id: 1,
  author: 'Owtp',
}

console.log(newPost(firstPost));


/*************************
  *************************
    ERROR HANDLING IN JS
  *************************
*************************/

const fnWithError = () => {
  throw new Error('Some error');
}


/* try {
  fnWithError();
} catch (error) {
  console.error(error);
  console.log(error.message);
}
 */
console.log('Continue...');


/*************************
  *************************
    ARRAYS
  *************************
*************************/

const myArr = [1, 2, 3];
console.log(myArr);

const myArr2 = new Array(1, 2, 3);
console.log(myArr2);

const myArr3 = [1, true, 'Owtp'];
console.log(myArr3.length);

const sevenInchesIn = 1;
const bloodOnTheFloor = [1, 3, 34, 5];

function is_array(item) {
  if (Array.isArray(item) === true) {
    return true;
  } else {
    return false;
  }
}

console.log(is_array(bloodOnTheFloor));

/*************************
  *************************
    DESTRUCTURING THE OBJECT 
  *************************
*************************/

const user = {
  isMarried: false,
  age: 20,
  name: 'owtp',
  commentQty: 30
}

const { isMarried, secondObject } = user;
const { age } = user;
console.log(isMarried);


const fruits = ['Apple', 'Orange', 'Grapefruit', 'Blueberry'];
const [fruitOne, fruitTwo, fruitMf] = fruits;
console.log(fruitMf);


const userInfo = ({ commentQty, name }) => {
  if (!commentQty) {
    return `User ${name} has no comments at all!`
  }
  return `User ${name} has ${commentQty} comments!`
}

console.log(userInfo(user));

/*************************
  *************************
    CONDITION INSTRUCTION
  *************************
*************************/

if (!user.name) {
  console.log('Name not found');
} else {
  console.log(`Name is ${String(user.name)}`);
}

console.log(!undefined >= NaN);

/* 
let ageOfPeople = 18;

if (ageOfPeople > 18) {
  console.log('Adult!');
} else if (ageOfPeople > 12) {
  console.log('Teenager!');
} else {
  console.log('Child!');
} 
*/


const sumPositiveNumbers = (a, b) => {
  if (typeof a != 'number' || typeof b != 'number') {
    return 'One or both of the arguments are not the number(s)';
  }

  if (a <= 0 || b <= 0) {
    return 'One or both of the arguments are not positive number(s)';
  }

  return a + b;
}

console.log(sumPositiveNumbers(0, 10));


/*************************
  *************************
    SWITCH
  *************************
*************************/

const month = 2;

switch (month) {
  case 12:
    console.log('December');
    break

  case 1:
    console.log('January')
    break

  case 2:
    console.log('February');
    break
  default:
    console.log('It\'s not a Winter month!');
}

/*************************
  *************************
    CONDITIONAL OPERATOR
  *************************
*************************/
let value = 11;
console.log(value >= 0 ? value : -value);

value = -5;
const res = value >= 0 ? value : -value;
console.log(res);



/*************************
  *************************
    MODULES
  *************************
*************************/

import increase from "./script.js";

console.log(increase(5, 5));


import {
  one as numberNine,
  two
} from "./script.js";

console.log(numberNine);
console.log(two)


/*************************
  *************************
    CLASSES
  *************************
*************************/
class Comment {

  constructor(text) {
    this.text = text;
    this.votesQty = 0;
    this.dislikesQty = 0;
  }

  upvote() {
    this.votesQty += 1;
  }

  dislike() {
    this.dislikesQty += 1;
  }

  static mergeComments(first, second) {
    return `${first} ${second}`;
  }
}


console.log(Comment.mergeComments('First Comment', 'Second Comment'))



const firstComment = new Comment('First Comment');

firstComment.upvote()
console.log(firstComment.votesQty);
firstComment.upvote()
console.log(firstComment.votesQty);

console.log(firstComment instanceof Object)
console.log(firstComment instanceof Comment)

console.log(firstComment.hasOwnProperty('diversity'));
console.log(firstComment.hasOwnProperty('upvote'));


const secondComment = new Comment('Second Comment');
secondComment.upvote();
secondComment.dislike();
console.log(secondComment.dislikesQty);
const thirdComment = new Comment('Third Comment');
thirdComment.upvote();
thirdComment.upvote();
thirdComment.upvote();
thirdComment.dislike();
console.log(thirdComment.votesQty);



class NumbersArray extends Array {
  sum() {
    return this.reduce((el, acc) => acc += el, 0);
  }
}

const myArray = new NumbersArray(2, 4, 5);
const arrThis = new NumbersArray(6, 7, 7, 8);


console.log(myArray)
console.log(myArray.sum())
console.log(arrThis.sum())

const defaultStatement = 'Even if you saying the truth, you can\'t be right';
const arrList = [1, 'no way', { name: 'Owtp', age: 20, isRich(a, b) { return a + b; } }, [defaultStatement, 5]];

arrList.forEach(item => {
  console.log(item);
})


class Product {
  constructor(name, price) {
    this.name = name;
    this.price = price;
  }

  displayProduct() {
    console.log(`Product: ${this.name}`);
    console.log(`Price: $${this.price}`);
  }
}

const product1 = new Product('Dishonored', 10.99);
product1.displayProduct();

/*************************
*************************
  PROMISES
*************************
*************************/
/* fetch('') */



/*************************
*************************
  TEST
*************************
*************************/
let myProfit = 1200000;
const debts = {
  Mom: 200000,
  Murad: 150000,
  Fazliddin: 150000,
  Rixsitilla: 60000,
}
let overallDebt = 0;

for (const key in debts) {
  const everyDebt = debts[key];

  overallDebt += everyDebt;
}

console.log(myProfit - overallDebt);